import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gc_store/src/components.dart';
import 'package:gc_store/src/configs.dart';

/// Main page of the "Grocery Store" Application.
class GroceryStore extends StatelessWidget {
  const GroceryStore({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeDataConfig().theme,
      home: Scaffold(
        appBar: GroceryStoreAppBar(),
        bottomNavigationBar: GroceryStoreBottomBar(
          context: context,
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
