import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers.dart';

/// Bottom bar with navigation items.
class GroceryStoreBottomBar extends BottomNavigationBar {
  GroceryStoreBottomBar({Key? key, required BuildContext context})
      : super(
          key: key,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.festival_outlined),
              label: 'Store',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.query_stats_outlined),
              label: 'Statistic',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: 'Favorite',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.qr_code),
              label: 'Promo',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart),
              label: 'Cart',
            ),
          ],
          currentIndex: context.watch<BottomBarPagesProvider>().pageIndex,
          onTap: (index) {
            context.read<BottomBarPagesProvider>().changePage(index);
          },
        );
}
