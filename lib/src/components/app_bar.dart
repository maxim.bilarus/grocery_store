import 'package:flutter/material.dart';

/// Top app bar with title.
class GroceryStoreAppBar extends AppBar {
  GroceryStoreAppBar({Key? key})
      : super(key: key, title: const Text('Grocery Store'));
}
