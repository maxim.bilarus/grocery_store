import 'package:flutter/material.dart';
import 'package:gc_store/src/configs.dart';
import 'colors_config.dart';

/// Main styles config.
class ThemeDataConfig {
  ThemeData get theme => ThemeData(
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          selectedItemColor: ColorsConfig.activeWhite,
          unselectedItemColor: ColorsConfig.bleedWhite,
          backgroundColor: ColorsConfig.lightGreen,
          type: BottomNavigationBarType.fixed,
        ),
        appBarTheme: AppBarTheme(
          backgroundColor: ColorsConfig.lightGreen,
          titleTextStyle: TextStyle(
            fontWeight: FontWeight.w900,
            fontSize: 22,
            color: ColorsConfig.activeWhite,
          ),
        ),
      );
}
