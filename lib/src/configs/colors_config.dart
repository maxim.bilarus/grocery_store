import 'package:flutter/material.dart';

/// Config with mostly used colors.
class ColorsConfig {
  static final lightGreen = Colors.green[300];
  static final bleedWhite = Colors.white54;
  static final activeWhite = Colors.white;
}
