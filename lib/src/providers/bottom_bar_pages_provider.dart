import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// Provides possibility to change pages using navigation bottom bar.
class BottomBarPagesProvider with ChangeNotifier, DiagnosticableTreeMixin {
  int pageIndex = 0;

  void changePage(int index) {
    pageIndex = index;
    notifyListeners();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IntProperty('pageIndex', pageIndex));
  }
}
