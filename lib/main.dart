import 'package:flutter/material.dart';
import 'package:gc_store/app.dart';
import 'package:gc_store/src/providers.dart';
import 'package:provider/provider.dart';

void main() {
  /// Start Application
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => BottomBarPagesProvider(),
        ),
      ],
      child: const GroceryStore(),
    ),
  );
}
